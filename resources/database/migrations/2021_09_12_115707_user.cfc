component {

	function up( schema, query ){
		schema.create( "users", function(table) {
			table.increments( "id" );
			table.string( "email" ).unique();
			table.string( "password" );
			table.string( "first_name" );
			table.string( "last_name" );
			table.string( "city" );
			table.string( "country" );
			table.string( "phone" );
			table.boolean("is_active");
			table.json("permissions");
			table.timestampTz( "created_date" );
			table.timestampTz( "last_updated" );
		} );
	}

	function down( schema, query ){
		schema.drop( "users" );
	}

}
