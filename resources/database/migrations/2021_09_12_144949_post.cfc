component {

    function up( schema, qb ) {
		schema.create( "posts", function( table ) {
			table.unsignedInteger( "user_id" )
					.references( "id" )
					.onTable( "users" )
					.onUpdate( "CASCADE" );
			table.increments("id");
			table.longText("content");
			table.lineString("title");
			table.mediumText("summary");
			table.timestampTz("date_created");
			table.timestampTz("last_updated");

		} );
    }

    function down( schema, qb ) {
		schema.drop("posts");
    }

}
