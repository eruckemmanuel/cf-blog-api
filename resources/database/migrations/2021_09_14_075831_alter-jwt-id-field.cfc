component {
    
    function up( schema, qb ) {
        schema.alter( "jwt_user_token", function( table ) {
            table.modifyColumn( "id", table.string( "id" ) );
            table.modifyColumn( "token", table.longText( "token" ) );
        });
        
    }

    function down( schema, qb ) {
        
    }

}
