component {

    function up( schema, qb ) {
		schema.create( "jwt_user_token", function(table) {
			table.increments( "id" );
			table.string( "cacheKey" );
			table.string( "token" );
			table.timestampTz( "issued" );
			table.timestampTz( "expiration" );
			table.string("subject");
		} );
    }

    function down( schema, qb ) {
		schema.drop("jwt_user_token");
    }

}
