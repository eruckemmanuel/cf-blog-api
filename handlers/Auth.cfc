/**
 * Authentication Handler
 */
component extends="coldbox.system.RestHandler" {

	// Injection
	property name="userService" inject="quickService:User";

	/**
	 * Login a user into the application
	 *
	 * @x-route (POST) /api/login
	 * @requestBody ~auth/login/requestBody.json
	 * @response-default ~auth/login/responses.json##200
	 * @response-401 ~auth/login/responses.json##401
	 */
	function login( event, rc, prc ) {
		if(userService.isValidCredentials(rc.email, rc.password)){
			var user = userService.retrieveUserByUsername(rc.email);
			event
				.getResponse().setData( {
					"token" : jwtAuth().fromuser( user ),
					"user"  : user.getMemento()
				} )
				.addMessage(
				"Bearer token created and it expires in #jwtAuth().getSettings().jwt.expiration# minutes"
				);

		}else{
			event.getResponse().setError(true).setStatusCode(401).addMessage("Invalid email or password");
		}
	}

	/**
	 * Register a new user in the system
	 *
	 * @x-route (POST) /api/register
	 * @requestBody ~auth/register/requestBody.json
	 * @response-default ~auth/register/responses.json##200
	 * @response-400 ~auth/register/responses.json##400
	 */
	function register( event, rc, prc ) {
		prc.oUser = userService.create( {
						"firstName": rc.firstName,
						"lastName": rc.lastName,
						"city": rc.city,
						"phone": rc.phone,
						"country": rc.country,
						"email": rc.email,
						"password": rc.password,
						"lastUpdated": now(),
						"createdDate": now(),
					} );

		// Log them in if it was created!
		event
			.getResponse()
			.setData( {
				"token" : jwtAuth().fromuser( prc.oUser ),
				"user"  : prc.oUser.getMemento()
			} )
			.addMessage(
				"User registered correctly and Bearer token created and it expires in #jwtAuth().getSettings().jwt.expiration# minutes"
			);
	}

	/**
	 * Logout a user
	 *
	 * @x-route (POST) /api/logout
	 * @security bearerAuth,ApiKeyAuth
	 * @response-default ~auth/logout/responses.json##200
	 * @response-500 ~auth/logout/responses.json##500
	 */
	function logout( event, rc, prc ) {
		jwtAuth().logout();
		event.getResponse().addMessage( "Successfully logged out" );
	}

	function listUsers(event, rc, prc){
		prc.oUsers = userService.all().$renderData();
		event.getResponse().setData(prc.oUsers);
	}


}
