component extends="coldbox.system.RestHandler" {
    
    property name="postService" inject="quickService:Post";

    function index(event, rc, prc){
        var posts = postService.all().$renderData();
        event.getResponse().setData(posts);
    }


    function create(event, rc, prc){
        var post = postService.create(
            {
                "title": rc.title,
                "summary": rc.summary,
                "content": rc.content,
                "lastUpdated": now(),
                "dateCreated": now()
            }
        );
        event.getResponse.setData(post);
    }
}