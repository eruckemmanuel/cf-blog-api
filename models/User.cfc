component extends="quick.models.BaseEntity" accessors="true" {

	/**
	 * --------------------------------------------------------------------------
	 * Properties
	 * --------------------------------------------------------------------------
	 */

    property name="id";
    property name="email";
    property name="firstName" column="first_name";
    property name="lastName" column="last_name";
    property name="createdDate" column="created_date";
    property name="lastUpdated" column="last_updated";
    property name="city";
    property name="country";
	property name="phone";
	property name="password";
	property name="isActive" column="is_active" default=1;
	property name="permissions" type="array" default=[];

	property name="bcrypt" inject="@BCrypt" persistent="false";
	property name="_meta" persistent="false";


	public User function setPassword( required string password ){
		return assignAttribute( "password", bcrypt.hashPassword( arguments.password ) );
	}


	/**
	 * --------------------------------------------------------------------------
	 * Mementifier
	 * --------------------------------------------------------------------------
	 */
	this.memento = {
		neverInclude    : [ "password" ]
	};



	function newCollection( array entities = [] ) {
		return variables._wirebox.getInstance(
			name = "quick.extras.QuickCollection",
			initArguments = {
				"collection" = arguments.entities
			}
			);
	}

	public boolean function isValidCredentials( required string email, required string password ){
		var user = newEntity().where( "email", arguments.email ).first();
		if ( isNull( user ) ) {
			return false;
		}
		return bcrypt.checkPassword( arguments.password, user.getPassword() );
	}

	public User function retrieveUserByUsername( required string email ){
		return newEntity().where( "email", arguments.email ).firstOrFail();
	}

	public User function retrieveUserById( required numeric id ){
		return newEntity().findOrFail( arguments.id );
	}


	/**
	 * --------------------------------------------------------------------------
	 * IJwtSubject Methods
	 * --------------------------------------------------------------------------
	 */

	/**
	 * A struct of custom claims to add to the JWT token
	 */
		struct function getJwtCustomClaims() {
			return {};
		}

	/**
	 * This function returns an array of all the scopes that should be attached to the JWT token that will be used for authorization.
	 */
		array function getJwtScopes() {
			return [];
		}


}
