component extends="quick.models.BaseEntity" accessors="true" {

	property name="id";
	property name="title";
	property name="summary";
	property name="content";
	property name="dateCreated" column="date_created";
	property name="lastUpdated" column="last_updated";


	function user() {
       return belongsTo( "User" );
    }

	
	function newCollection( array entities = [] ) {
		return variables._wirebox.getInstance(
			name = "quick.extras.QuickCollection",
			initArguments = {
				"collection" = arguments.entities
			}
			);
	}

}
