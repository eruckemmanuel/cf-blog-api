# CF Blog API

Blog API with ColdFusion

Source Code

- https://gitlab.com/eruckemmanuel/cf-blog-api

## Installation

```bash
cd cf-blog-api
```

```bash
box install
```

This will setup all the needed dependencies for application.

Run server

```bash
box server start
```
